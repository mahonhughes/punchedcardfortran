﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace punchedcardFORTRAN
{

    //Thank you, Thomas Saunders for your wonderful articles about trains.I am due to make a state visit to the united kingdom of great Britain and northern Ireland.As i am a very humble dictator, i shall be using the public transport system, namely the trains.Although I am a nice dictator, I am still a dictator and thus i will be travelling first class on the trains.I am a little worried, however, as i don't see any turkmen food on the first class virgin menu. This will be a slight issue for me. Another major issue for me is a lack of first class on the london underground, Please write me an article on that horrible injustice. Hopefully, by the time of my state visit arrival, there will be a first class underground carriage. Thank you again, Gurbanguly Berdimuhamedow, President of Turkmenistan.

    //Рахмет, Томас Саундерс пойыздар туралы тамаша мақалаларыңыз үшін. Мен Біріккен Корольдіктің және Солтүстік Ирландияның біріккен Корольдігіне мемлекеттік сапармен баруым керек. Мен өте қарапайым диктатор болғандықтан, мен қоғамдық көліктік жүйені, атап айтқанда, пойыздарды пайдаланамын. Мен жақсы диктатор болсам да, мен әлі де диктатормын, сондықтан пойыздарда бірінші сыныпқа барамын. Алайда, мен бірінші сыныптағы тың мәзірде ешқандай түрік тағамдарын көрмейтіндіктен, аздап алаңдамаймын. Бұл мен үшін аз ғана мәселе болады. Мен үшін тағы бір маңызды мәселе - Лондондағы метрополитенде бірінші сыныптың жетіспеушілігі. Бұл қорқынышты әділетсіздік туралы маған жазыңыз. Менің мемлекеттік сапарым келген сәтте бірінші сыныпты жер асты тасымалдайтын болады деп үміттенемін. 

    //Түрікменстан Президенті,
    //Гурбангулы Бердімұхамедовқа.



    class Program
    {

        struct fortVar
        {
            public string name;
            public string type;
            public string stringData;
        };
        static Dictionary<string, fortVar> varDict = new Dictionary<string, fortVar>();
        static ConsoleColor defaultColor = ConsoleColor.White;
        static int width = 80;
        static int height = 12;
        static bool debugMode = false;
        static void Main(string[] args)
        {
            Console.BackgroundColor = defaultColor;
            Console.ForegroundColor = ConsoleColor.Black;
            ConsoleColor[,] colorMatrix = new ConsoleColor[80, 12];
            bool[,] punchedMatrix = new bool[80, 12];
            
            Console.CursorVisible = false;
            for (int i = 0; i < 12; i++)
            {
                for (int b = 0; b < 80; b++)
                {
                    colorMatrix[b, i] = defaultColor;
                }
            }

            Console.OutputEncoding = System.Text.Encoding.Unicode;
            //Punch(punchedMatrix, colorMatrix);
            //buildCard(colorMatrix, punchedMatrix);

            Queue<string> programLines = new Queue<string>();
            programLines.Enqueue("string::pete=hello");
            //programLines.Enqueue("hello = hello*hello");
            programLines.Enqueue("print *, pete");
            if (debugMode)
            {
                Console.WriteLine("count:" + programLines.Count);
            }
             
                //punchStudio(punchedMatrix, colorMatrix, programLines);

                executeFortran(programLines);

            Console.Read();
        }


        static void punchStudio(bool[,] punchedMatrix, ConsoleColor[,] colorMatrix, Queue<string> programLines)
        {
            //Queue<string> programLines = new Queue<string>();
            while (true)
            {
                Console.CursorLeft = 0;
                Console.CursorTop = 0;

                Console.WriteLine("N: New card");
                Console.WriteLine("E: Finish reading cards and Execute");
                Console.WriteLine("Q: Quit");

                ConsoleKeyInfo keyinfo;
                keyinfo = Console.ReadKey();
                if (keyinfo.Key == ConsoleKey.N)
                {
                    programLines.Enqueue(Punch(punchedMatrix, colorMatrix));
                }
                else if(keyinfo.Key == ConsoleKey.E)
                {
                    executeFortran(programLines);
                }
                else if(keyinfo.Key == ConsoleKey.Q)
                {
                    System.Environment.Exit(1);
                }
            }
        }
        static double logic(string block)
        {
            if (block.Split('\'').Length>1)
            {
                Error("Invalid Logic");
            }
            for (int i = 0; i < block.Length; i++)
            {
                if (block[i] == '*' && i < block.Length - 1)
                {
                    if (block[i+1] == '*')
                    {
                        char[] tempCharArray = block.ToCharArray();
                        //Console.WriteLine("tca"+ new string(tempCharArray));
                        tempCharArray[i] = '^';
                        tempCharArray[i+1] = ' ';
                        //Console.WriteLine("tca" + new string(tempCharArray));
                        //block = String.Join("" + tempCharArray);
                        block = new string(tempCharArray);
                    }
                }
            }
            //Console.WriteLine("block" + block);
            string[] pieces = block.Split(' ', '+', '-', '*', '/', '^');
            string[] spacesRemoved = block.Split(' ');
            string[] operations = new string[spacesRemoved.Length];
            Queue<char> operationsQueue = new Queue<char>();
            Queue<string> partQueue = new Queue<string>();
            int currentPiece = 0;
            double answer = 0;
            double running = 0;
            if (debugMode)
            {
                //Console.WriteLine("pieces[1]" + pieces[1]);
            }
            //if (varDict.ContainsKey(pieces[currentPiece]))
            //{
            //running = double.Parse(varDict[pieces[1]].stringData);
            //}
            //else
            //{
            //    running = double.Parse(pieces[1]);
            //}
            //currentPiece += 1;
            //for (int b = 0; b < block.Length; b++)
            //{

            //}
            for (int i = 0; i < block.Length; i++)
            {
                if (block[i] == '+'|| block[i] == '-' || block[i] == '/' || block[i] == '*' || block[i] == '^')
                {
                    if (debugMode)
                    { Console.WriteLine(block[i]); }
                    operationsQueue.Enqueue(block[i]);
                }
            }
            for (int i = 0; i < pieces.Length; i++)
            {
                if (pieces[i] != "")
                {
                    partQueue.Enqueue(pieces[i]);
                }   
            }
            //Console.WriteLine("partQueue.Peek()"+ partQueue.Peek());
            if (varDict.ContainsKey(partQueue.Peek()))
            {
                running = double.Parse(varDict[partQueue.Dequeue()].stringData);
            }
            else
            {
                running = double.Parse(partQueue.Dequeue());
            }
            int loopNumber = partQueue.Count;
            for (int i = 0; i < loopNumber; i++)
            {
                double currentNumber = 0;
                char currentOperation = operationsQueue.Dequeue();
                if (varDict.ContainsKey(partQueue.Peek()))
                {
                    currentNumber = double.Parse(varDict[partQueue.Dequeue()].stringData);
                }
                else
                {
                    currentNumber = double.Parse(partQueue.Dequeue());
                }
                if (currentOperation == '+')
                {
                    running += currentNumber;
                    if (debugMode) { Console.WriteLine("plus"); }
                }
                else if (currentOperation == '-')
                {
                    running = (running - currentNumber);
                    if (debugMode)
                    { Console.WriteLine("minus"); }
                }
                else if (currentOperation == '/')
                {
                    running = (running / currentNumber);
                    if (debugMode)
                    { Console.WriteLine("divide"); }
                }
                else if (currentOperation == '*')
                {
                    running = (running * currentNumber);
                    if (debugMode)
                    { Console.WriteLine("multiply"); }
                }
                else if (currentOperation == '^')
                {
                    running = Math.Pow(running, currentNumber);
                    if (debugMode)
                    { Console.WriteLine("power"); }
                }
            }




            //for (int i = 0; i < block.Length; i++)
            //{
            //    double currentNumber = 0;
                
            //    if (varDict.ContainsKey(pieces[currentPiece]))
            //    {
            //        currentNumber = double.Parse(varDict[pieces[currentPiece]].stringData);
            //    }
            //    else if (pieces[currentPiece] == " " || pieces[currentPiece] == "")
            //    {
            //        currentPiece++;
                    
            //    }
            //    else
            //    {
            //        currentNumber = double.Parse(pieces[currentPiece]);
            //    }

            //    if (block[i] == '+')
            //    {
            //        running += currentNumber;
            //        currentPiece++;
            //        Console.WriteLine("plus");
            //    }
            //    else if (block[i] == '-')
            //    {
            //        running = (running - currentNumber);
            //        currentPiece++;
            //        Console.WriteLine("minus");
            //    }
            //    else if (block[i] == '/')
            //    {
            //        running = (running / currentNumber);
            //        currentPiece++;
            //        Console.WriteLine("divide");
            //    }
            //    else if (block[i] == '*')
            //    {
            //        running = (running * currentNumber);
            //        currentPiece++;
            //        Console.WriteLine("multiply");
            //    }
            //    Console.WriteLine("c" + currentNumber);
            //    Console.WriteLine("r" + running);
            //    Console.WriteLine("currentPiece" + currentPiece);
            //    Console.WriteLine("pieces[currentPiece]"+ pieces[currentPiece]);
            //    for (int b = 0; b < pieces.Length; b++)
            //    {
            //        Console.WriteLine("pieces[b]"+ pieces[b]);
            //    }
            //}

            //for (int i = 1; i < block.Length; i++)
            //{
            //    Console.WriteLine("Block:" + block);
            //    Console.WriteLine("pieces[currentPiece]" + pieces[currentPiece]);
            //    Console.WriteLine("running" + running);
            //    Console.WriteLine(varDict.ContainsKey(pieces[currentPiece]));
            //    Console.WriteLine("i" + i);
            //    //Console.WriteLine(varDict["hello"].stringData);
            //    //Console.WriteLine(Convert.ToString(varDict[pieces[i]].stringData));
            //    double currentNumber = 0;
            //    //Console.WriteLine("pieces[currentPiece]" + pieces[currentPiece]);
            //    if (pieces[currentPiece] == " " || pieces[currentPiece] == "")
            //    {
            //        //currentPiece += 1;
            //    }
            //    else if (varDict.ContainsKey(pieces[currentPiece]))
            //    {
            //        currentNumber = double.Parse(varDict[pieces[currentPiece]].stringData);
            //        //currentPiece += 1;
            //    }
            //    else
            //    {
            //        currentNumber = double.Parse(pieces[currentPiece]);
            //        //currentPiece += 1;
            //    }
            //    Console.WriteLine("currentNumber" + currentNumber);
            //    //Console.WriteLine("pieces[currentPiece]"+ pieces[currentPiece]);
            //    //currentPiece += 1;
            //    if (block[i] == '+')
            //    {
            //        running += currentNumber;
            //        currentPiece++;
            //        Console.WriteLine("plus");
            //    }
            //    else if (block[i] == '-')
            //    {
            //        running = (running - currentNumber);
            //        currentPiece++;
            //        Console.WriteLine("minus");
            //    }
            //    else if (block[i] == '/')
            //    {
            //        running = (running / currentNumber);
            //        currentPiece++;
            //        Console.WriteLine("divide");
            //    }
            //    else if (block[i] == '*')
            //    {
            //        running = (running * currentNumber);
            //        currentPiece++;
            //        Console.WriteLine("multiply");
            //    }
            //}
            answer = running;
            //string firstLogic = pieces[0] + pieces[1] + pieces[2];
            
            return answer;
        }



        static string fortInput()
        {
            return Console.ReadLine();  
        }





        static void fortPrint(string line)
        {
            string p1 = line.Split(',')[1];
            //p1 = p1.Split(' ')[1];
            string p2 = p1;
            if (debugMode)
            {
                Console.WriteLine("p1:" + p1);
            }

            if (p1[0] == ' ')
            {
                p2 = p1.Split(' ')[1];
            }


            if (line.Split('+', '-', '*', '/').Length > 1)
            {
                Console.WriteLine(logic(p1));
            }
            else
            {



                //if (varDict.ContainsKey(p1))
                //{
                //    Console.WriteLine(varDict[p1].stringData);
                //}
                if (line.Split('\'').Length > 1)
                {
                    if (p2[0] == '\'')
                    {
                        string print = line.Split('\'','*')[1];
                        Console.WriteLine(print);
                    }
                }
            }
        }

        


        static void executeFortran(Queue<string> programLines)
        {
            for (int i = 0; i < programLines.Count+1; i++)
            {
                string line = programLines.Dequeue();
                string firstWord = line.Split(' ').First();
                if (debugMode)
                {
                    Console.WriteLine("count:" + programLines.Count);
                    //Console.WriteLine(line);
                    //Console.WriteLine(line.Split('=').Length);
                }
                string[] decCheck = line.Split(':');
                fortVar fortVariable;
                if (debugMode)
                {
                    Console.WriteLine("Line:" + line);
                    Console.WriteLine("firstWord:" + firstWord);
                    Console.WriteLine("count:" + programLines.Count);
                    Console.WriteLine("decCheck: " + (decCheck.Length>1));
                }

                if (firstWord == "print")
                {
                    fortPrint(line);
                }
                else if (decCheck.Length > 1)
                {
                    Queue<string> partQueue = new Queue<string>();
                    string[] isEquals = line.Split('=');
                    string[] splitString = line.Split('=',':',' ','\'');
                    for (int b = 0; b < splitString.Length; b++)
                    {
                        if (splitString[b] != "")
                        {
                            partQueue.Enqueue(splitString[b]);
                        }
                    }
                    
                    fortVariable.type = partQueue.Dequeue();
                    fortVariable.name = partQueue.Dequeue();
                    if (debugMode)
                    {
                        Console.WriteLine("type:" + fortVariable.type);
                        Console.WriteLine("name:" + fortVariable.name);
                        Console.WriteLine("isEquels:" + (isEquals.Length > 1));
                    }
                    if (isEquals.Length > 1)
                    {
                        
                        fortVariable.stringData = partQueue.Dequeue();
                        
                    }
                    else
                    {
                        fortVariable.stringData = "";
                    }
                    varDict.Add(fortVariable.name, fortVariable);
                    if (debugMode)
                    {
                        Console.WriteLine("stringData:" + fortVariable.stringData);
                    }
                }
                else if (line.Split('=').Length>1)
                {
                    //Console.WriteLine("DE C");
                    if (varDict.ContainsKey(line.Split(' ','=')[0]))
                    {
                        fortVariable.type = varDict[line.Split(' ', '=')[0]].type;
                        fortVariable.name = line.Split(' ')[0];
                        varDict.Remove(line.Split(' ','=')[0]);
                    }
                    else
                    {
                        Error("Variable not defined.");
                    }
                    //Console.WriteLine(line.Split(' ', '=')[2]);

                    //varDict[line.Split(' ')[0]].stringData = "";
                    fortVariable.name = line.Split(' ')[0];
                    fortVariable.stringData = line.Split(' ','=')[2];
                } 


            }
        }


        static string Punch(bool[,] punchedMatrix, ConsoleColor[,] colorMatrix)
        {
            string[,] selectMatrix = new string[80, 12];
            int[] currentSelection = new int[2];
            currentSelection[0] = 0;
            currentSelection[1] = 0;
            string selectionType = "X";

            while (true)
            {
                for (int i = 0; i < 12; i++)
                {
                    for (int b = 0; b < 80; b++)
                    {
                        if (punchedMatrix[b, i])
                        {
                            colorMatrix[b, i] = ConsoleColor.Black;
                        }
                    }
                }
                selectMatrix[currentSelection[0], currentSelection[1]] = selectionType;
                colorMatrix[currentSelection[0], currentSelection[1]] = ConsoleColor.Red;
                buildCard(colorMatrix, punchedMatrix);
                //for (int i = 0; i < 12; i++)
                //{
                //    for (int b = 0; b < 80; b++)
                //    {
                //        colorMatrix[b, i] = defaultColor;
                //    }
                //}
                ConsoleKeyInfo keyinfo;
                keyinfo = Console.ReadKey();
                if (keyinfo.Key == ConsoleKey.UpArrow)
                {
                    if (currentSelection[1] >= 1)
                    {
                        colorMatrix[currentSelection[0], currentSelection[1]] = defaultColor;
                        currentSelection[1] = currentSelection[1] - 1;
                        selectionType = "X";
                    }
                }
                else if (keyinfo.Key == ConsoleKey.DownArrow)
                {
                    if (currentSelection[1] < height - 1)
                    {
                        colorMatrix[currentSelection[0], currentSelection[1]] = defaultColor;
                        currentSelection[1] = currentSelection[1] + 1;
                        selectionType = "X";
                    }
                }
                else if (keyinfo.Key == ConsoleKey.LeftArrow)
                {
                    if (currentSelection[0] > 0)
                    {
                        colorMatrix[currentSelection[0], currentSelection[1]] = defaultColor;
                        currentSelection[0] = currentSelection[0] - 1;
                        selectionType = "X";
                    }
                }
                else if (keyinfo.Key == ConsoleKey.RightArrow)
                {
                    if (currentSelection[0] < width - 1)
                    {
                        colorMatrix[currentSelection[0], currentSelection[1]] = defaultColor;
                        currentSelection[0] = currentSelection[0] + 1;
                        selectionType = "X";
                    }
                }
                else if (keyinfo.Key == ConsoleKey.Enter)
                {
                    if (punchedMatrix[currentSelection[0], currentSelection[1]] == false)
                    {
                        punchedMatrix[currentSelection[0], currentSelection[1]] = true;
                        colorMatrix[currentSelection[0], currentSelection[1]] = ConsoleColor.Black;
                    }
                    else
                    {
                        punchedMatrix[currentSelection[0], currentSelection[1]] = false;
                    }
                }
                else if (keyinfo.Key == ConsoleKey.I)
                {
                    return RunPunchedcard(punchedMatrix);
                }
            }
        }

        static void buildCard(ConsoleColor[,] colorMatrix, bool[,] punchedMatrix)
        {
            
            Console.CursorLeft = 0;
            Console.CursorTop = 0;
            Console.Write("┍");
            for (int i = 0; i < 80; i++)
            {
                Console.Write("━");
            }
            Console.Write("┑");
            Console.Write("\n");
            for (int i = 0; i < 2; i++)
            {
                Console.Write("│");
                for (int b = 0; b < 80; b++)
                {
                    Console.BackgroundColor = colorMatrix[b,i];
                    Console.Write(" ");
                    Console.BackgroundColor = defaultColor;
                }
                Console.Write("│");
                Console.Write("\n");
            }
            
            for (int b = 2; b < 12; b++)
            {
                Console.Write("│");
                for (int i = 0; i < 80; i++)
                {
                    Console.BackgroundColor = colorMatrix[i, b];
                    if (punchedMatrix[i,b] == false)
                    {
                        Console.Write(b-2);
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                    Console.BackgroundColor = defaultColor;
                }
                Console.Write("│");
                Console.Write("\n");

            }
            Console.Write("┕");
            for (int i = 0; i < 80; i++)
            {
                Console.Write("━");
            }
            Console.Write("┙");
            Console.CursorLeft = 0;
            Console.CursorTop = 0;

        }

        static string RunPunchedcard(bool[,] punchedMatrix)
        {
            Console.Clear();
            string line = "";
            char currentChar = '0';
            for (int i = 0; i < 80; i++)
            {
                int manyint = 0;
                for (int b = 0; b < 12; b++)
                {
                    if (punchedMatrix[i,b])
                    {
                        manyint++;
                    }
                }
                if (manyint == 0)
                {
                    currentChar = ' ';
                }
                else if (manyint == 1)
                {
                    int charInt = 0;
                    for (int b = 0; b < 12; b++)
                    {
                        if (punchedMatrix[i, b] == true)
                        {
                            if (b == 0)
                            {
                                currentChar = '+';
                            }
                            else if (b==1)
                            {
                                currentChar = '-';
                            }
                            else
                            {
                                charInt = b - 2;
                                currentChar = (Convert.ToChar(Convert.ToString(charInt)));
                            }
                        }
                    }
                }
                else if (manyint == 2)
                {
                    int setterInt = 0;
                    int charInt = 0;
                    bool isOne = true;
                    
                    for (int b = 0; b < 12; b++)
                    {
                        if (punchedMatrix[i, b] == true && isOne == true)
                        {
                            setterInt = b;
                            isOne = false;
                        }
                        else if(punchedMatrix[i, b] == true && isOne == false)
                        {
                            charInt = b;
                        }
                    }


                    if (charInt == 10)
                    {
                        if(setterInt == 5)
                        {
                            currentChar = '=';
                        }
                        else if (setterInt == 6)
                        {
                            currentChar = '\'';
                        }
                    }
                    else if (setterInt == 0)
                    {
                        char tempChar = 'a';
                        for (int k = 0; k < charInt - 3; k++)
                        {
                            tempChar++;
                        }
                        currentChar = tempChar;
                    }
                    else if (setterInt == 1)
                    {
                        char tempChar = 'j';
                        for (int k = 0; k < charInt - 3; k++)
                        {
                            tempChar++;
                        }
                        currentChar = tempChar;
                    }
                    else if (setterInt == 2)
                    {
                        if (charInt == 3)
                        {
                            char tempChar = '/';
                            currentChar = tempChar;
                        }
                        else
                        {
                            char tempChar = 'r';
                            for (int k = 0; k < charInt - 3; k++)
                            {
                                tempChar++;
                            }
                            currentChar = tempChar;
                        }
                    }
                }
                else if (manyint == 3)
                {
                    int setterInt = 0;
                    int charInt = 0;
                    bool isOne = true;

                    for (int b = 0; b < 12; b++)
                    {
                        if (punchedMatrix[i, b] == true && isOne == true)
                        {
                            setterInt = b;
                            isOne = false;
                        }
                        else if (punchedMatrix[i, b] == true && isOne == false)
                        {
                            charInt = b;
                            break;
                        }
                    }

                    if (setterInt == 0)
                    {
                        if (charInt == 5)
                        {
                            currentChar = '.';
                        }
                        else if (charInt == 6)
                        {
                            currentChar = ')';
                        }
                    }
                    else if (setterInt == 1)
                    {
                        if (charInt == 5)
                        {
                            currentChar = '$';
                        }
                        else if (charInt == 6)
                        {
                            currentChar = '*';
                        }
                    }
                    else if (setterInt == 2)
                    {
                        if (charInt == 5)
                        {
                            currentChar = ',';
                        }
                        else if (charInt == 6)
                        {
                            currentChar = '(';
                        }
                    }

                }
                else
                {
                    Error("Invalid amount of punches");
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                
                Console.Write(currentChar);
                line += currentChar;
                
            }
            Console.WriteLine("\n" + line);
            Console.Read();
            return line;
        }
        static void Error(string error)
        {
            Console.WriteLine(error);
        }
    }
}
